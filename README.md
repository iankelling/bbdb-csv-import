This project has moved to https://sr.ht/~iank/bbdb-csv-import/

Sourcehut is better from an ethical perspective.

gitlab.com requires running nonfree google recaptcha to register. Most
of the server code is not free. The primary purpose of its development
and features are to sell nonfree software. It requires running
javascript programs for important functionality.

